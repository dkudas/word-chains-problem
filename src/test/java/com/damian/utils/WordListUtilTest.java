package com.damian.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Damian on 2017-04-27.
 */
public class WordListUtilTest {

    private static final String WORD_1 = "A'asia";
    private static final String WORD_2 = "ABS";
    private static final String WORD_3 = "Damian jest fajny";

    @Test
    public void checkListWordsIsCorrect() {
        List<String> words = new WordListUtil().getWordList();
        Assert.assertTrue(words.contains(WORD_1));
        Assert.assertTrue(words.contains(WORD_2));
        Assert.assertFalse(words.contains(WORD_3));
    }
}
