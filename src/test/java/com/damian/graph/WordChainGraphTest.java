package com.damian.graph;

import com.damian.utils.WordListUtil;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Damian on 2017-04-27.
 */
@RunWith(DataProviderRunner.class)
public class WordChainGraphTest {

    @DataProvider
    public static Object[][] goodData() {
        return new Object[][]{
                {Arrays.asList(new String[]{"cat", "mama", "lolo", "cot", "dzidzia", "dot", "dog"}),
                        Arrays.asList(new String[]{"cat", "cot", "dot", "dog"}), "cat", "dog"},
                {Arrays.asList(new String[]{"lol", "col", "biba", "a", "cul"}),
                        Arrays.asList(new String[]{"lol", "col", "cul"}), "lol", "cul"},
                {Arrays.asList(new String[]{"ah", "oh"}),
                        Arrays.asList(new String[]{"ah", "oh"}), "ah", "oh"},
        };
    }

    @DataProvider
    public static Object[][] badData() {
        return new Object[][]{
                {Arrays.asList(new String[]{"tak", "mama", "dog"}), "tak", "nie"},
                {Arrays.asList(new String[]{"dom", "bom", "zam"}), "tak", "zam"},
                {Arrays.asList(new String[]{"cat", "cot", "dot"}), "cat", "dog"},
        };
    }

    @Test
    @UseDataProvider("goodData")
    public void checkChainIsCorrect(final List<String> dictionary, final List<String> expectedList,
                                    final String startWord, final String endWord) {
        WordListUtil util = mock(WordListUtil.class);

        when(util.getWordList()).thenReturn(dictionary);

        WordChainGraph wordChainGraph = new WordChainGraph();
        wordChainGraph.setDictionary(util.getWordList());
        List<String> chain = wordChainGraph.findShortestWordsChain(startWord, endWord);
        Assert.assertTrue(chain.equals(expectedList));
    }

    @Test
    @UseDataProvider("badData")
    public void checkChainDoesntExists(final List<String> dictionary, final String startWord, final String endWord) {
        WordListUtil util = mock(WordListUtil.class);

        when(util.getWordList()).thenReturn(dictionary);

        WordChainGraph wordChainGraph = new WordChainGraph();
        wordChainGraph.setDictionary(util.getWordList());
        List<String> chain = wordChainGraph.findShortestWordsChain(startWord, endWord);
        Assert.assertNull(chain);
    }

}
