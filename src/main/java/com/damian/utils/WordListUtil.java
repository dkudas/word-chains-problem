package com.damian.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2017-04-27.
 */
public class WordListUtil {

    private static final String PATH = "http://codekata.com/data/wordlist.txt";

    public List<String> getWordList() {
        List<String> wordList = new ArrayList<>();
        BufferedReader in = null;
        try {
            URL codekataUrl = new URL(PATH);
            in = new BufferedReader(new InputStreamReader(codekataUrl.openStream()));
            String word;
            while ((word = in.readLine()) != null)
                wordList.add(word);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return wordList;
    }
}
