package com.damian;

import com.damian.graph.WordChainGraph;
import com.damian.utils.WordListUtil;

import java.util.List;

/**
 * Created by Damian on 2017-04-27.
 */
public class Main {

    public static void main(String args[]) {

        if (args.length != 2) {
            System.err.println("Usage: java Main [word1] [word2]");
            System.exit(-1);
        }

        String startWord = args[0];
        String endWord = args[1];
        if (startWord.length() != endWord.length()) {
            System.err.println("Words haven't the same length");
            System.exit(-1);
        }

        WordChainGraph wordChainGraph = new WordChainGraph();
        wordChainGraph.setDictionary(new WordListUtil().getWordList());
        List<String> chain = wordChainGraph.findShortestWordsChain(startWord, endWord);
        if (chain != null) {
            chain.stream().forEach(s -> System.out.println(s));
        } else {
            System.err.println("Chain from " + startWord + " to " + endWord + " doesn't exists");
        }
    }
}
