package com.damian.graph;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Damian on 2017-04-27.
 */
public class WordChainGraph {

    private List<String> dictionary;

    public List<String> findShortestWordsChain(final String startWord, final String endWord) {

        if (!dictionary.contains(startWord) || !dictionary.contains(endWord)) {
            return null;
        }

        Map<String, String> parent = new HashMap<>();
        Queue<String> queue = new LinkedList<>();
        queue.add(startWord);

        boolean isChainFinished = false;
        while (!queue.isEmpty() && !isChainFinished) {
            String word = queue.remove();
            if (word.equals(endWord)) {
                isChainFinished = true;
            } else {
                List<String> childWords = getChildrenForWord(word);
                for (String child : childWords) {
                    if (!parent.containsKey(child)) {
                        queue.add(child);
                        parent.put(child, word);
                    }
                }
            }
        }
        return getChain(parent, startWord, endWord);
    }

    private List<String> getChildrenForWord(final String word) {

        return dictionary.stream().filter(x -> {
            int diff = 0;
            if (x.length() != word.length()) {
                return false;
            }
            for (int i = 0; i < word.length(); i++) {
                if (x.charAt(i) != word.charAt(i)) {
                    diff++;
                }
            }
            return diff == 1;
        }).collect(Collectors.toList());
    }

    private List<String> getChain(final Map<String, String> parent, final String startWord, final String endWord) {
        if (!parent.containsKey(endWord)) {
            return null;
        }

        List<String> chain = new LinkedList<>();
        String word = endWord;
        while (!word.equals(startWord)) {
            chain.add(0, word);
            word = parent.get(word);
        }

        chain.add(0, startWord);
        return chain;
    }

    public List<String> getDictionary() {
        return dictionary;
    }

    public void setDictionary(List<String> dictionary) {
        this.dictionary = dictionary;
    }
}
